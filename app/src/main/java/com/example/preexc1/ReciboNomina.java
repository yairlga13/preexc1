package com.example.preexc1;

import java.io.Serializable;
import java.util.Random;

public class ReciboNomina implements Serializable {

    private static final long serialVersionUID = 1L;

    private int numeroRecibo;
    private String nombre;
    private int horasNormales;
    private int horasExtras;
    private int puesto;
    private double porcentajeImpuesto;

    public ReciboNomina(){
        this.numeroRecibo= 0;
        this.nombre="";
        this.horasExtras=0;
        this.horasNormales=0;
        this.puesto=0;
        this.porcentajeImpuesto=0.0f;
    }

    public ReciboNomina(int numeroRecibo, String nombre, int horasNormales, int horasExtras, int puesto, double porcentajeImpuesto) {
        this.numeroRecibo = numeroRecibo;
        this.nombre = nombre;
        this.horasNormales = horasNormales;
        this.horasExtras = horasExtras;
        this.puesto = puesto;
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public ReciboNomina(ReciboNomina nomina){
        this.numeroRecibo=nomina.numeroRecibo;
        this.nombre= nomina.nombre;
        this.horasExtras=nomina.horasExtras;
        this.horasNormales=nomina.horasNormales;
        this.puesto=nomina.puesto;
        this.porcentajeImpuesto=nomina.porcentajeImpuesto;
    }

    public int getNumeroRecibo() {
        return numeroRecibo;
    }

    public void setNumeroRecibo(int numeroRecibo) {
        this.numeroRecibo = numeroRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getHorasNormales() {
        return horasNormales;
    }

    public void setHorasNormales(int horasNormales) {
        this.horasNormales = horasNormales;
    }

    public int getHorasExtras() {
        return horasExtras;
    }

    public void setHorasExtras(int horasExtras) {
        this.horasExtras = horasExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public double getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(double porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public double calcularPagoBase() {
        double pagoBase = 200.0;
        switch (puesto) {
            case 1:
                return pagoBase + pagoBase * 0.20;
            case 2:
                return pagoBase + pagoBase * 0.50;
            case 3:
                return pagoBase + pagoBase * 1.00;
            default:
                throw new IllegalArgumentException("Puesto inválido, Ingresa numero del 1 al 3");
        }
    }

    public double calcularSubtotal() {
        double pagoBase = calcularPagoBase();
        return (horasNormales * pagoBase) + (horasExtras * pagoBase * 2);
    }

    public double calcularImpuesto() {
        double subtotal = calcularSubtotal();
        return subtotal * (porcentajeImpuesto / 100);
    }

    public double calcularTotalAPagar() {
        double subtotal = calcularSubtotal();
        double impuesto = calcularImpuesto();
        return subtotal - impuesto;
    }

    public int generaId() {
        Random r = new Random();
        return r.nextInt() % 1000;
    }

}
